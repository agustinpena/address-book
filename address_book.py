# Address Book project (MySQL, PyMySQL)
# this script edits an existing database
# called 'address_book_db', with one table called 'contacts'
# and columns 'name', 'surname', 'phone_number',
# 'email'. Access the database locally, root user,
# no password.
# Required modules: pymysql, tabulate

# THINGS TO DO:
# +. display in alphabetical order?
# +. add text to the 'except' command in line 38
# +. TRY/EXCEPT WILL NOT WORK (IDE ISSUE?)
###################################################

# import required module(s)
import pymysql
from tabulate import tabulate

# define a few needed structures
fields = {'n': 'name', 's': 'surname', 'p': 'phone number', 'e': 'email'}
hdrs = ['Name', 'Surname', 'Phone No', 'Email']
hdrs_id = ['ID', 'Name', 'Surname', 'Phone No', 'Email']
options = {'n': 'name', 's': 'surname', 'p': 'phone_number', 'e': 'email'}
op_sign = '[(m)ain, (s)how all, (f)ind, (a)dd, (u)pdate, (d)elete, (q)uit]: '
cmd_id = '''SELECT name, surname, phone_number, email FROM contacts
WHERE ct_id = '{}';'''

# define a fuction which retrieves a row
# by means of its id number


def get_res_id(id):
    cr.execute('''SELECT name, surname, phone_number, email
    FROM contacts WHERE ct_id = {};'''.format(id))
    res_id = cr.fetchall()
    return res_id

# define a function yielding  a list with all contact id numbers
# when the search is open (only name or surname is provided)


def open_search(n, s):
    results = ()
    if n != '' and s == '':
        cr.execute('''SELECT * FROM contacts WHERE
        name = '{}';'''.format(n))
        results = cr.fetchall()
    elif n == '' and s != '':
        cr.execute('''SELECT * FROM contacts WHERE
        surname = '{}';'''.format(s))
        results = cr.fetchall()
    elif n != '' and s != '':
        cr.execute('''SELECT * FROM contacts WHERE
        name = '{}' AND surname = '{}';'''.format(n, s))
        results = cr.fetchall()
    return results


# create connection and cursor objects
try:
    db = pymysql.connect('localhost', 'root', '', 'address_book_db')
    cr = db.cursor()
except():
    print('')
    print('-' * 40)
    print('MySQL SERVER IS NOT RUNNING.')
    print('Please start the server and run the program again.')
    print('-' * 40)
    print('')
    quit()

print('')
print('--------------------')
print('   Address Book')
print('--------------------')
print('')

# Start main loop
optn = 'm'
while True:
    if optn == 'm':
        print('Main Menu:')
        print('-> [m]ain menu')
        print('-> [s]how the entire address book')
        print('-> [f]ind and display an entry')
        print('-> [a]dd a new entry')
        print('-> [u]pdate an entry')
        print('-> [d]elete an entry')
        print('-> [q] to exit the program')
        print('')
        optn = input('Enter an option: ')
        print('')

    elif optn == 'f':
        # find and display an entry
        nm = input('Enter name: ')
        sn = input('Enter surname: ')
        # retrieves results
        results = open_search(nm, sn)
        # print results
        if results != ():
            results_no_id = [x[1:] for x in results]
            print('')
            print(tabulate(results_no_id, headers=hdrs, tablefmt='grid'))
        else:
            print('')
            print('THE SEARCH RETURNED NO RESULTS')
        print('')
        print('Enter a main option')
        optn = input(op_sign)
        print('')
    elif optn == 's':
        # SHOW THE ENTIRE ADDRESS BOOK
        # retreive the address book
        cr.execute('SELECT name, surname, phone_number, email FROM contacts;')
        results = cr.fetchall()
        # retrieve the total number of rows
        cr.execute('SELECT COUNT(name) FROM contacts;')
        row_count = cr.fetchall()
        # print results
        print('')
        print(tabulate(results, headers=hdrs, tablefmt='grid'))
        print('TOTAL NUMBER OF CONTACTS: ' + str(row_count[0][0]))
        print('')
        print('Enter a main option')
        optn = input(op_sign)
        print('')

    elif optn == 'd':
        # DELETE AN ENTRY
        # retrieve possible results
        print('You want to delete an entry.')
        nm = input('Enter name: ')
        sn = input('Enter surname: ')
        results = open_search(nm, sn)
        # determine the right id_number
        if len(results) == 0:
            print('NO SUCH ENTRY')
        elif len(results) == 1:
            id_num = results[0][0]
        else:
            print(tabulate(results, headers=hdrs_id, tablefmt='grid'))
            while True:
                print('Enter the ID number of the entry to delete')
                idn = int(input('Enter ID no: '))
                list_ids = [x[0] for x in results]
                if idn in list_ids:
                    id_num = idn
                    break
                else:
                    print('')
                    print('PLEASE ENTER A VALID ID NUMBER')
        # delete the entry
        single_list = [x[1:] for x in results if x[0] == id_num]
        print(tabulate(single_list, headers=hdrs, tablefmt='grid'))
        print('ARE YOU SURE YOU WANT TO DELETE THIS ENTRY?')
        while True:
            dt = input('[Enter (d)elete, or press ENTER to cancel]: ')
            if dt == 'd':
                cr.execute('''DELETE FROM contacts WHERE
                    ct_id={}'''.format(id_num))
                db.commit()
                print('')
                print('The entry has been deleted')
                break
            elif dt == '':
                break
            else:
                print('Please enter a valid option')
        # ask for a new main option
        print('')
        print('Enter a main option')
        optn = input(op_sign)
        print('')
    elif optn == 'a':
        # ADD A NEW ENTRY
        print('Enter the required fields.')
        nm = input('Name: ')
        sn = input('Surname: ')
        pn = input('Phone number: ')
        eml = input('Email: ')
        cmd = '''INSERT INTO contacts(name, surname, phone_number, email)
                values('{}', '{}', '{}', '{}');'''
        cr.execute(cmd.format(nm, sn, pn, eml))
        db.commit()
        print('')
        print('Entry has been added')
        print('')
        print('Enter a main option')
        optn = input(op_sign)
        print('')
    elif optn == 'u':
        # UPDATE AN ENTRY
        # retrieve possible results
        print('You want to update an entry')
        nm = input('Enter name: ')
        sn = input('Enter surname: ')
        results = open_search(nm, sn)
        # determine the right id number
        if len(results) == 0:
            print('NO SUCH ENTRY')
        elif len(results) == 1:
            id_num = results[0][0]
        else:
            print(tabulate(results, headers=hdrs_id, tablefmt='grid'))
            while True:
                print('Enter the ID number of the entry to update')
                idn = int(input('Enter ID no: '))
                list_ids = [x[0] for x in results]
                if idn in list_ids:
                    id_num = idn
                    break
                else:
                    print('Please enter a valid id number')
            # determine the field to update
        if len(results) != 0:
            print('')
            single_list = [x[1:] for x in results if x[0] == id_num]
            print(tabulate(single_list, headers=hdrs, tablefmt='grid'))
            print('WHAT FIELD DO YOU WANT TO UPDATE?')
            while True:
                print('[(n)ame, (s)urname, (p)hone number, (e)mail]')
                fl = input('Press ENTER to cancel: ')
                if fl == '':
                    break
                elif fl in options.keys():
                    vl = input('Enter the new ' + fields[fl] + ': ')
                    cr.execute('''UPDATE contacts
                    SET {} = '{}'
                    WHERE ct_id = {};'''.format(options[fl], vl, id_num))
                    db.commit()
                    # show updated results
                    upres = get_res_id(id_num)
                    print('')
                    print('ENTRY IS UPDATED')
                    print(tabulate(upres, headers=hdrs, tablefmt='grid'))
                    break
                else:
                    print('Please enter a valid field')
            else:
                pass
        print('')
        print('Enter a main option')
        optn = input(op_sign)
    elif optn == 'q':
        # quit the program
        break
    else:
        print('PLEASE ENTER A VALID OPTION')
        print('')
        print('Enter a main option')
        optn = input(op_sign)
